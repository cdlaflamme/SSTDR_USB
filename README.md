
# SSTDR_USB

Uses a USB sniffer to intercept and process data from a [Livewire Innovation](https://www.livewireinnovation.com/sstdr/) SSTDR device.
This is mainly useful for the following reasons:
* Data can easily be collected (with the press of a button rather than an entire file saving dialogue)
* Data can be collected with timestamps (good for long-term monitoring or data collection)
* Data can be obtained and processed in real-time (good for fault detection)

# Setup and Usage

There are two main ways to run this software. One is to trun it like any other python program, requiring one to install python and all dependencies. This is necessary only if one wishes to alter the code themselves. The other option is to run a pre-bundled executable version obtained from the [releases page](https://gitlab.com/cdlaflamme/SSTDR_USB/-/releases). There are some setup steps required no matter how the software is being run.

## Required Setup

These steps are required, no matter how the software is being run. They only ever need to be performed once on any machine that runs the software.
1. **Install [USBPcap](https://desowin.org/usbpcap/).** This is the USB sniffer used to intercept data and must be installed in the default location. A reboot is required after this step.
2. **Install Livewire Studio.** This software comes from livewire innovation and is used to control their SSTDR devices.
3. **Add libusb-1.0.dll to the system PATH.** The included libusb-1.0.dll is used by the backend of certain python USB libraries. This file can be added to path in many ways, but the simplest is to simply move a copy into System32.

## Starting the Software

These steps need to be performed (approximately) every time the software is launched.
1. **Plug in the SSTDR device.** Connect it to the computer using USB. It is generally wise to connect it to the wireing network under test first, as the USB connection is tenuous.
2. **Start Livewire Studio.** This software will configure and control the SSTDR device.
3. **Enable "Raw Data Format."** This is easy to forget and must be done every time. This setting can be found in the Livewire settings (which is found in the bottom right of the interface).
4. **Start an intermittent test.** Livewire Studio can be used to configure the settings of an intermittent test. This will cause the device to collect data as fast as possible with the provided settings. This test can be left running in the background, and does not need to be restarted if the SSTDR_USB software is closed.
5. **Launch SSTDR_USB.** This is done via a command prompt, with slight variations depending on whether the software is being run via python or the executable.
    - **Python**: Run the program like any other python code. Navigate to the directory and run with `python SSTDR_USB.py`.
    - **Executable (Windows)**: Run like any other executable. Navigate to the directory and run with `SSTDR_USB.exe`.

## Features and Usage

Once the software is opened, keys on the keyboard can be used to perform various functions. Basic data collection controls are shown on the interface, but other controls exist (mostly related to fault detection).

### GUI controls

* Escape: Exit the software.
* B: designates the most recently received waveform as the baseline.
* A: designates the most recently received waveform as the terminal measurement (referred to sometimes as the "A+" measurement. Only used for the METHOD_BLS_DEVIATION_CORRECTION fault detection method, which is not recommended).
* T: recalculates reflection width using the current baseline deviation threshold, baseline and terminal measurements.
* Left/Right: Decreases/Increases the baseline deviation threshold (only used in certain fault detection methods).
* W: Takes a single set of measurements (default 10, modified by `-mcount` argument). "W" stands for "window".
* L: Toggles nonstop data collection. Takes data as fast as possible unless `-interval` argument is supplied.
* I: Increases the log number manually.

### Terminal Controls

* Q: Exits the software.
* M: Takes a single set of measurements (default 10, modified by `-mcount` argument). "W" stands for "window".
* L: Toggles nonstop data collection. Takes data as fast as possible unless `-interval` argument is supplied.

### Output File Format

The raw data is saved in CSV files, which can be opened in excel or a common text editor. These files can easily be opened in matlab using `csvread` or `readmatrix`, or with numpy using `np.genfromtxt`.

Each row represents an SSTDR measurement, with the following layout:

* Column 1: The session number. If the SSTDR software is launched multiple times, outputting to the same file each time, each time the software is launched a new "session" begins. The log number goes back to 0 for each session, but all data saved in the same session will have a unique session number.
* Column 2: The log number. This increments every time a group of measurements is taken. Every measurement with the same log number was taken with the wiring network under test in the same state. In other words, multiple measurements with the same log number are theoretically identical, with the only differences being from noise or very fast time-varying factors.
* Column 3: The measurement timestamp as a floating point number in epoch time (the number of seconds since January 1st, 1970. Very common; also called unix time).
* Columns 4+: The unprocessed SSTDR waveform. Provided by the device as 16-bit signed integers. The sample count depends on certain intermittent test settings (frequency and chip size seem to be related). Often between 90 and 110 samples.

## Command Line Arguments

### -yaml [path]

**Aliases**: -y<br>
**Default**: default.yaml

Directs the software towards a specific panel configuration file.
This option is only useful with the GUI enabled. The GUI displays a sample solar string with a certain number of panels in a particular layout, and knows what distance is between the panels for the purpose of displaying faults in their correct location in the string. This YAML file provides all the necessary details about the string, such as panel count, layout, and distances.

### -mcount [integer]

**Default**: 10

Determines how many measurements are taken when the "take measurement" button is pressed. The default is 10 so measurements can be averaged together and ignore noise.

### -maxsize [integer]

**Default**: 1GB

The maximum filesize in Bytes of an output file. Only used if autoout mode is enabled.

### -filter [integer]

**Aliases**: -f

Manually determines the filter USBPcap is to listen on. This is normally determined automatically and should not generally be used.

### -address [integer]

**Aliases**: -a

Manually determines the device address USBPcap is to listen to. This is normally determined automatically and should not generally be used.

### -bus [integer]

**Aliases**: -b

Manually determines the device bus USBPcap is to listen on. This is normally determined automatically and should not generally be used.

### -file [path]

Enables file mode (default: off), where input is received from a file rather than an SSTDR device. The path argument directs the software towards the desired file (accepting files in the same format as those output by this software).

### -bli [integer]

Short for "BaseLine Index." Optional argument used in file mode. Tells the software what index / row contains the baseline measurement. If this is provided, fault detection can be performed in file mode.

### -tli [integer]

**Aliases**: -ti

Short for "TerminaL Index." Optional argument used in file mode. Tells the software what index / row contains the terminal measurement. If this is provided, fault detection can be performed in file mode using the METHOD_BLS_DEVIATION_CORRECTION method (not recommended).

### -out [path]

**Aliases**: -o<br>
**Default**: SSTDR_waveforms.csv

Specifies the file path the software will output to. If auto-out mode is enabled, this is the name of a directory. Otherwise, it is the name of a CSV file.

### -interval [integer (see description)]

**Aliases:** -i, -t

Determines the "time interval" for measurements, in seconds. Normally, when logging/measurement is toggled on, data will be taken as fast as possible. If this argument is provided, measurements will instead be taken every time this time interval passes (for example, passing 1 will cause a measurement to be taken every second). Instead of an integer, one can pass in "h" or "hour" for 3600, "m", "min", or "minute" for 60, and "s" or "second" for 1.

### -gui

Enables the GUI (default on in most versions). The gui presents an image of the string, faults if fault detection is enabled, and some clickable buttons for taking measurements.

### -nogui

**Aliases**: -no-gui

Disables the GUI (default on in most versions). The gui presents an image of the string, faults if fault detection is enabled, and some clickable buttons for taking measurements.

### -waveform

**Aliases**: -wf, -v

Enables waveform visualization (default on in most versions). This widow presents a plot of the most recently received SSTDR waveform. If a baseline has been specified, this will display the most recent waveform minus the baseline (i.e. the most recent waveform with baseline subtraction performed).

### -no-waveform

**Aliases**: -nwf, -nv

Disables waveform visualization (default on in most versions). This widow presents a plot of the most recently received SSTDR waveform.

### -autoout

Enables auto-out mode (default off in most versions). This mode changes the output path to be a directory, and outputs to numbered files within that directory. Data starts being output to a new file once the current file hits the maximum filesize (1GB by default). This is helpful if one is storing data over long periods of time, to reduce memory constraints when loading data files.
